//
//  CreateContactViewController.swift
//  AddressBook
//
//  Created by Lee Li Wei on 07/05/2019.
//  Copyright © 2019 Lee Li Wei. All rights reserved.
//

import UIKit
import CoreData

class CreateContactViewController: UIViewController {

    @IBOutlet weak var createEmailField: UITextField!
    @IBOutlet weak var createNameField: UITextField!
    
    @IBOutlet weak var createPhoneField: UITextField!
    
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    @IBAction func cancelCreate(_ sender: Any) {
        
        
    }
    
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let fetchResultCtrl = NSFetchedResultsController<NSFetchRequestResult>();
    var recordID = 1;
    
    var nameFieldIsValid = false;
    var phoneFieldIsValid = false;
    var emailFieldIsValid = false;


    override func viewDidLoad() {
        super.viewDidLoad()
        
        createEmailField.text = ""
        createNameField.text = ""
        createPhoneField.text = ""
        
        saveBtn.isEnabled = false
        
        
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contacts")
        let sortDescriptor = NSSortDescriptor(key: "id" , ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.fetchLimit = 1
        
        do {
            let results = try self.managedObjectContext.fetch(fetchRequest)
            
            if(results.count == 1) {
                recordID = Int((results[0] as AnyObject).id  + 1)
                
            } else {
                recordID = 1
            }
            
            print("Current Record ID: \(recordID)")
        } catch {
            print("Unable to retrieve record ID");
        }
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func nameFieldHandler(_ sender: Any) {
        guard let text = createNameField.text else { return }
        if !text.isEmpty {
            nameFieldIsValid = true;
        } else {
            nameFieldIsValid = false
        }
        validator()
    }
    
    @IBAction func emailFieldHandler(_ sender: Any) {
//        if (!createEmailField.text!.isEmpty()){
//            hasError = false;
//        }
        
        guard let text = createEmailField.text else { return }
        if !text.isEmpty {
        
            emailFieldIsValid = true;

        } else {
            emailFieldIsValid = false;
        }
        validator()
    }
    
    
    @IBAction func phoneFieldHandler(_ sender: Any) {
        guard let text = createPhoneField.text else { return }
        if !text.isEmpty {
            phoneFieldIsValid = true;

        } else {
            phoneFieldIsValid = false
        }
        validator()
    }
    
    func validator(){
        if(emailFieldIsValid && phoneFieldIsValid && nameFieldIsValid){
            saveBtn.isEnabled = true
        } else {
            saveBtn.isEnabled = false

        }
    }
    
    @IBAction func createContact(_ sender: Any) {
        let entity = NSEntityDescription.entity(forEntityName: "Contacts", in: managedObjectContext)
        let newContact = Contacts(entity: entity!, insertInto: managedObjectContext)
        newContact.id = Int64(recordID)
        newContact.name = createNameField.text!
        newContact.phone = createPhoneField.text!
        newContact.email = createEmailField.text!
    
        
            do {
                try managedObjectContext.save()
                dismiss();
            } catch _ {
                print("Can't Save")
            }
    
       
    }
    
    func dismiss(){
        navigationController?.popViewController(animated: true);
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
