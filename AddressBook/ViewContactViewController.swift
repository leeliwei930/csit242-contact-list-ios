//
//  ViewContactViewController.swift
//  AddressBook
//
//  Created by Lee Li Wei on 08/05/2019.
//  Copyright © 2019 Lee Li Wei. All rights reserved.
//

import UIKit
import CoreData

class ViewContactViewController: UIViewController {
    
    var contact: Contacts? = nil
    
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var fetchResultCtrl = NSFetchedResultsController<NSFetchRequestResult>()
    let fetchResult = NSFetchRequest<NSFetchRequestResult>(entityName: "Contacts")
    @IBOutlet weak var idPlaceholder: UILabel!
    
    @IBOutlet weak var namePlaceholder: UILabel!
    @IBOutlet weak var emailPlaceholder: UILabel!
    
    @IBOutlet weak var phonePlaceholder: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(contact != nil){
            idPlaceholder.text = "\(String(describing: contact!.id))"
            namePlaceholder.text = contact?.name
            emailPlaceholder.text = contact?.email
            phonePlaceholder.text = contact?.phone
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("View will appear called")
        
        fetchResult.predicate = NSPredicate(format: "id == %@", "\(contact!.id)")
        
        do {
            let myContact = try managedObjectContext.fetch(fetchResult);
            let contactData = myContact.first as! Contacts;
            
            namePlaceholder.text = contactData.name
            emailPlaceholder.text = contactData.email
            phonePlaceholder.text = contactData.phone
        } catch {
            
        }
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "editContact"){
            
            
            let editContactCtrl: EditContactViewController = segue.destination as! EditContactViewController
            
            editContactCtrl.contact = contact
        }
    }
 

}
