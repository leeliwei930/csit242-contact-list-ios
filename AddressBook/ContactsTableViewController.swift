//
//  ContactsTableViewController.swift
//  AddressBook
//
//  Created by Lee Li Wei on 08/05/2019.
//  Copyright © 2019 Lee Li Wei. All rights reserved.
//

import UIKit
import CoreData

class ContactsTableViewController: UITableViewController, NSFetchedResultsControllerDelegate, UISearchBarDelegate {
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var fetchResultCtrl = NSFetchedResultsController<NSFetchRequestResult>();
    
    var searchMode = false;
    var textModel = "";
    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchResultCtrl = getFetchedResultsController()
        fetchResultCtrl.delegate = self
        searchBar.delegate = self
        do {
            try fetchResultCtrl.performFetch()
        } catch _ {
            print("Error on fetching data")
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchResultCtrl = getFetchedResultsController()
        do {
            try self.fetchResultCtrl.performFetch()
        } catch _ {
            print("Error on fetching data")
        }
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        // perform search
        if searchText.isEmpty {
            self.searchMode = false
            self.fetchResultCtrl = getFetchedResultsController()
            do {
                try self.fetchResultCtrl.performFetch()
            } catch _ {
                print("Error on fetching data")
            }

        } else {
            self.searchMode = true;
            self.textModel = searchText;
            print("searching")
            self.fetchResultCtrl = getFetchedResultsController()
            do {
                try self.fetchResultCtrl.performFetch()
                
            } catch _ {
                print("Error on fetching search data")
            }
        }
        tableView.reloadData()
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchMode = false;
        self.fetchResultCtrl = getFetchedResultsController()

        tableView.reloadData();
    }
    
    func getFetchedResultsController() -> NSFetchedResultsController<NSFetchRequestResult> {
        if(!self.searchMode){
            
            fetchResultCtrl = NSFetchedResultsController(fetchRequest: listFetchRequest() , managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)

        } else {
            
            fetchResultCtrl = NSFetchedResultsController(fetchRequest: searchContactRequest() , managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)

        }
        
        return fetchResultCtrl
    }
    
    
    func listFetchRequest() -> NSFetchRequest<NSFetchRequestResult> {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contacts")
        let sortDescriptor = NSSortDescriptor(key: "id" , ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest
        
    }
    
    func searchContactRequest() -> NSFetchRequest<NSFetchRequestResult> {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contacts")
        let sortDescriptor = NSSortDescriptor(key: "id" , ascending: true)
        fetchRequest.predicate = NSPredicate(format: "name CONTAINS[cd] %@", textModel)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest
    }
    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        let count =  fetchResultCtrl.sections?.count == nil ? 0 : fetchResultCtrl.sections?.count;
//        return count!
//    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        self.fetchResultCtrl = getFetchedResultsController()
        do {
            try self.fetchResultCtrl.performFetch()
        } catch _ {
            print("Error on fetching data")
        }
        let numberOfRowsInSection = fetchResultCtrl.sections?[section].numberOfObjects
        return numberOfRowsInSection!
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath)
            let item = fetchResultCtrl.object(at: indexPath as IndexPath) as! Contacts
        
            let email = String(item.email!);
            let name = String(item.name!);
            let phone = String(item.phone!);
            cell.textLabel?.text = "\(name) - \(email)"
            cell.detailTextLabel?.text = phone
            // Configure the cell...
        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let manageObject: NSManagedObject = fetchResultCtrl.object(at: indexPath) as! NSManagedObject
            
            managedObjectContext.delete(manageObject)
            
            do {
                try managedObjectContext.save()
            } catch _ {
                print("Unable to delete")
            }
            tableView.deleteRows(at: [indexPath], with: .automatic)
//            tableView.reloadData()
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.reloadData()
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "viewContact"){
            let cell = sender as! UITableViewCell
            let indexPath = tableView.indexPath(for: cell)
            let itemCtrl: ViewContactViewController = segue.destination as! ViewContactViewController
            let contactInfo : Contacts = fetchResultCtrl.object(at: indexPath!) as! Contacts
            itemCtrl.contact = contactInfo
        }
    }
 

}
