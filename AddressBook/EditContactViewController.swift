//
//  EditContactViewController.swift
//  AddressBook
//
//  Created by Lee Li Wei on 08/05/2019.
//  Copyright © 2019 Lee Li Wei. All rights reserved.
//

import UIKit
import CoreData


class EditContactViewController: UIViewController {
    
    var contact: Contacts? = nil
    
    
    @IBOutlet weak var updateBtn: UIBarButtonItem!
    @IBOutlet weak var updateNameField: UITextField!
    @IBOutlet weak var updateEmailField: UITextField!
    @IBOutlet weak var updatePhoneField: UITextField!
    var nameFieldIsValid = false;
    var phoneFieldIsValid = false;
    var emailFieldIsValid = false;
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBtn.isEnabled = false

        if(contact != nil){
            updateNameField.text = contact?.name
            updateEmailField.text = contact?.email
            updatePhoneField.text = contact?.phone
            updateBtn.isEnabled = true
            nameFieldIsValid = true
            phoneFieldIsValid = true
            emailFieldIsValid = true
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func updateNameFieldHandler(_ sender: Any) {
        guard let text = updateNameField.text else { return }
        if !text.isEmpty {
            nameFieldIsValid = true;
        } else {
            nameFieldIsValid = false
        }
        validator()
    }
    
    @IBAction func updateEmailFieldHandler(_ sender: Any) {
        guard let text = updateEmailField.text else { return }
        if !text.isEmpty {
            
            emailFieldIsValid = true;
            
        } else {
            emailFieldIsValid = false
        }
        validator()
    }
    
    @IBAction func updatePhoneFieldHandler(_ sender: Any) {
        guard let text = updatePhoneField.text else { return }
        if !text.isEmpty {
            phoneFieldIsValid = true;
            
        } else {
            phoneFieldIsValid = false
        }
        validator()
    }
    
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var fetchResultCtrl = NSFetchedResultsController<NSFetchRequestResult>()
    
    
    @IBAction func cancelUpdate(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func updateContact(){
        contact?.name = updateNameField.text
        contact?.phone = updatePhoneField.text
        contact?.email = updateEmailField.text
        
        do {
            try managedObjectContext.save()
            navigationController?.popViewController(animated: true)
        } catch _ {
            print("can't update")
        }
    }
    
    func validator(){
        if(emailFieldIsValid && phoneFieldIsValid && nameFieldIsValid){
            updateBtn.isEnabled = true
        } else {
            updateBtn.isEnabled = false
        }
    }
    @IBAction func updateBtnHandler(_ sender: Any) {
        updateContact()
    }
}
